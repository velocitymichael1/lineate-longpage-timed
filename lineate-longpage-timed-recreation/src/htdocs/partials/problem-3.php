<section class="mt-25">
    <div class="container">

      
        <div class="row ">
                <div class="col-12 d-flex flex-column justify-content-center align-items-center header-background-problem-3">
                    <p>Major Problem #3</p>
                    <h2 >The knowledge-in-practice gap.</h2>
                </div>
        </div>
    

        <div class="row d-flex justify-content-center">
            <div class="col-10">
                <article class="<?php echo $white_paper_classes['white_container']; ?>">
                    <h4 class="mb-4">Expertise isn’t as standardized in actuality as it is on a skills sheet. Having technical know-how is one thing - but being able to actually apply that knowledge across a range of varying contexts is another.</h4>
                    
                    <div class="row">
                        <div class="<?php echo $white_paper_classes['text-column']; ?> ">
                            <p class="mb-2">So, what happens when the actual skills of your outsourced team don’t match up to the requirements of the project?</p>
                            <p class="mb-2">What if they’re prepared to offer boilerplate solutions for a project that requires custom architecture, custom data pipelining, custom thinking?</p>
                            <p class="mb-2">What if the needs of the project grow beyond the outsourced team’s technical capabilities?</p>
                            <p class="mb-2">And what if your time spent hand-holding is more trouble than it’s worth?</p>
                        </div>

                        <div class="<?php echo $white_paper_classes['image-column']; ?> ">    
                            <img class="white-container-image blue-image-border img-fluid" src="images/GettyImages-1180183363-GRADED 1.jpg"/>
                        </div>
                    </div>

                    <p class="mt-6"><strong>Again, this is where the soft skills and intangibles like a capacity for problem-solving add up. Companies can find out the hard way that technical expertise isn’t always what it says on the tin.</strong></p>
                </article>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <article class="<?php echo $blue_paper_classes['blue_container']; ?>">
                    <h2 class="mb-4">How we cracked it:</h2>

                    <?php //left aligned row?>
                    <div class="<?php echo $blue_paper_classes['left_aligned_row']; ?>">
                        <div class="<?php echo $blue_paper_classes['left_row_image_column']; ?>">
                            <div class="blue-image-holder d-flex justify-content-center align-items-center py-3">
                                <img height="160" width="160" src="images/outline-lineate-icon-difficult.svg" class="img-fluid"/>
                            </div>
                        </div>
                        <div class="<?php echo $blue_paper_classes['left_row_text_column']; ?>">
                            <h4 class="mb-2">We live for the most difficult work.</h4>
                            <p>Head-banging, ultra-complex software problems - they’re kind of our thing. That means not only applying our team’s world-class technical expertise, but knowing how and where it’s best applied.</p>
                        </div>
                    </div>

                    <?php //Right aligned row?>
                    <div class="<?php echo $blue_paper_classes['right_aligned_row']; ?>">
                        <div class="<?php echo $blue_paper_classes['right_row_text_column']; ?>">
                            <h4 class="mb-2">Custom means custom.</h4>
                            <p>Our Lockstep™ method means we go DEEP into the details of your project. You won’t find a trace of one-size-fits-all in our methodology.</p>
                            <p>The time we spend with you in discovery accelerates everything. More than just brainstorming the technical details - we immerse ourselves in how your business works. This lets us quickly go from a high-level proposal to a superbly precise plan.</p>
                            <p>Oh, and we don’t consider a project a success unless it’s truly future-proof. High standards? You betcha.</p>

                        </div>
                        <div class="<?php echo $blue_paper_classes['right_row_image_column']; ?>">
                            <div class="blue-image-holder d-flex justify-content-center align-items-center py-3">
                                <img height="180" width="160" src="images/outline-lineate-icon-custom.svg"/>
                            </div>
                        </div>
                    </div>

                    <?php //Left aligned row?>
                    <div class="<?php echo $blue_paper_classes['left_aligned_row']; ?>">
                        <div class="<?php echo $blue_paper_classes['left_row_image_column']; ?>">
                            <div class="blue-image-holder d-flex justify-content-center align-items-center py-3">
                                <img height="160" width="160" src="images/outline-lineate-icon-knowledge.svg"/>
                            </div>
                        </div>
                        <div class="<?php echo $blue_paper_classes['left_row_text_column']; ?>">
                            <h4 class="mb-2">We leave you more knowledgable.</h4>
                            <p>You’ve heard the old axiom: good code is documented out. We take that a few dozen steps further. We’re not just talking a few comments - we keep our processes recorded, tasks documented, plan changes marked out.</p>
                            <p>Simply put: we want your internal team to understand what we’ve built once we’ve left. Building out from what we’ve crafted should be easy. Even if we’re not the ones tasked with future work - we care that whoever undertakes it has a clear trail to follow.</p>
                            
                        </div>
                    </div>
                </article>
            </div>
        </div>
    </div>  
</section>