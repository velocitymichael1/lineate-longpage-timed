<nav>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center ">
                <div class="col-6 ">
                    <div class="logo-container ">
                        <img class="img-fluid" src="images/lineate_reverse_medium 1.png"/>
                    </div>
                </div>
                <div class="col-6 ">
                    <div class=" d-flex d-lg-inline justify-content-end">
                        <ul class="links d-none d-lg-flex justify-content-between align-items-center mt-2" >
                            <a>Who we are</a>
                            <a>What we do</a>
                            <a>Our work</a>
                            <a>Our community</a>
                            <img src="images/icon-search.svg" height="40" width="40"/>
                        </ul>
                        <button id="hamburger" class="d-flex  flex-column d-lg-none justify-content-center align-items-center mr-3">
                            <span class="mb-1"></span>
                            <span ></span>
                            <span class="mt-1"></span>
                        </button>
                    </div>
                </div>
                <div class="mobile-menu-container col-12 d-flex d-lg-none">
                    <div id="mobile-menu" class="mobile-menu links mobile-toggle mobile-menu d-lg-none flex-column align-items-center justify-content-center mt-3">
                        <a class="mb-1">Who we are</a>
                        <hr/>
                        <a class="mb-1">What we do</a>
                        <hr/>
                        <a class="mb-1">Our work</a>
                        <hr/>
                        <a>Our community</a>
                    </div>
                </div> 
        </div>
    </div>
</nav>