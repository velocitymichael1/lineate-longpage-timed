<section class="mt-10 mt-lg-25 pb-6">
    <div class="container">
        <div class="text-hero-inner-container d-flex flex-column flex-md-row justify-content-center ">
            <div class="row ">
                    <div class="col-12 col-md-6 col-lg-4 mb-2 mb-md-0">
                        <h3>Outsourcing is typically a headache.</h3>
                    </div>
                    <div class="col-12 col-md-6 col-lg-8">
                        <p>That might sound a bit odd coming from a company that specializes in outsourced services - but hey, we’ve got to reckon with the facts.</p>
                        <p>Maybe it’s more accurate to say that if mishandled, outsourcing can be a source of profound frustration, distended project timelines and ill-spent resources. </p>
                        <p>Of course, recognizing the common reasons why outsourced projects fail is the first step towards building ones that don’t. </p>
                        <p>So, even from the earliest days of Lineate, we knew we’d have to apply our obsession for problem-solving to the issues inherent in outsourcing. So, we did. And it worked.</p>
                        <p>Let’s dive into the three major problems companies tend to run into when outsourcing work (and we’ll spill on how we solved them.) </p>
                    </div>
            </div>
        </div>
    </div>
</section>









