<section class="hero-background mt-4">
    <div class="row">
        <div class="col-6">
            
        </div>
        <div class="col-12 d-flex d-lg-inline justify-content-center col-lg-6">
            <div class="hero-image-container d-flex justify-content-center align-items-center">
                <img height="700" width="840" class="hero-image img-fluid" src="images/hero-image.jpg"/>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="hero-inner-container  pt-8 pb-8">
                <div class="col-12 col-lg-6">
                    <h1>The 3 Major Problems With Outsourcing</h1>
                    <h4>(And How We Fixed Them)</h4>
                </div>
            </div>
        </div>
    </div>
</section>