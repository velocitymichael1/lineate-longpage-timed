<section class="mt-25">
    <div class="container">

    
        <div class="row ">
                <div class="col-12 d-flex flex-column justify-content-center align-items-center header-background-problem-2 ">
                    <p>Major Problem #2</p>
                    <h2 >Slowdowns.</h2>
                </div>
        </div>


        <div class="row d-flex justify-content-center">
            <div class="col-10">
                <article class="<?php echo $white_paper_classes['white_container']; ?>">
                    <p class="mb-2">The decentralized nature of outsourcing means that projects are often faced with speed bumps - issues that bog down the pace of collaborative work.</p>
                    <p class="mb-2">Granted, as the world continues a hefty pivot towards remote work, we’re all kinda getting better at it. However - there are levels to the remote collaboration game.</p>
                    <h4 class="mb-4">The best outsourcers aren't just the most knowledgeable or talented - they're masters of performing from afar. Isolated excellence loses momentum across borders. If that fact is ignored, your project won’t exactly be flitting along at a smooth pace.</h4>
                    <div class="row">
                        <div class="<?php echo $white_paper_classes['image-column']; ?> pr-lg-3 pr-xl-2">    
                            <img class="white-container-image green-image-border img-fluid" src="images/GettyImages-607477471-GRADED 1.jpg"/>
                        </div>
                        <div class="<?php echo $white_paper_classes['text-column']; ?> pl-lg-4 pl-xl-2  pt-2 pt-lg-0 ">
                            <p>Say, for example - if your outsourced and in-house teams are coordinating across time zones. Your people run into a high-impact blocker at 4PM local time, but your outsourced team is off the clock. If neither team is prepared for asynchronous problem-solving, it can devolve into a staggered back-and-forth effort that weighs down the pace of your project and puts deadline stress on everyone involved. </p>

                        </div>
                    </div>
                    <p class="mt-6"><strong>Or sometimes, there’s a change in business need that requires a deft pivot on an aspect of the project. Communicating that initiative across asynchronous teams can eat away at time. Taking the relevant issues and distilling them into cold, hard tasks that the outsourced devs can work with could almost be a job unto itself... *cue foreshadowing*</strong></p>

                </article>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <article class="<?php echo $blue_paper_classes['blue_container']; ?>">
                    <h2 class="mb-4">How we cracked it:</h2>

                    <?php //left aligned row?>
                    <div class="<?php echo $blue_paper_classes['left_aligned_row']; ?>">
                        <div class="<?php echo $blue_paper_classes['left_row_image_column']; ?>">
                            <div class="blue-image-holder d-flex justify-content-center align-items-center py-3">
                                <img height="160" width="160" src="images/outline-lineate-icon-flexibility.svg" class="img-fluid"/>
                            </div>
                        </div>
                        <div class="<?php echo $blue_paper_classes['left_row_text_column']; ?>">
                            <h4 class="mb-2">A focus on flexibility.</h4>
                            <p>We begin projects by maximizing overlapping time - gently leaning our schedules towards hours that fit our client best. As time passes and that becomes less necessary, we implement a tried-and-true format of regular asynchronous standups.</p>
                        </div>
                    </div>

                    <?php //Right aligned row?>
                    <div class="<?php echo $blue_paper_classes['right_aligned_row']; ?>">
                        <div class="<?php echo $blue_paper_classes['right_row_text_column']; ?>">
                            <h4 class="mb-2">We develop with soft skills in mind.</h4>
                            <p>From Day 1 of onboarding a new hire, we’re developing their soft skill set. We know that soft skills are a crucial ingredient for skilled specialists performing well. You simply can't isolate expertise from empathy in outsourcing - you need the hard stuff (deep domain knowledge) and the soft stuff (being a cultural chameleon).</p>
                        </div>
                        <div class="<?php echo $blue_paper_classes['right_row_image_column']; ?>">
                            <div class="blue-image-holder d-flex justify-content-center align-items-center py-3">
                                <img height="180" width="160" src="images/outline-lineate-icon-dev.svg"/>
                            </div>
                        </div>
                    </div>

                    <?php //Left aligned row?>
                    <div class="<?php echo $blue_paper_classes['left_aligned_row']; ?>">
                        <div class="<?php echo $blue_paper_classes['left_row_image_column']; ?>">
                            <div class="blue-image-holder d-flex justify-content-center align-items-center py-3">
                                <img height="160" width="160" src="images/outline-lineate-icon-refining.svg"/>
                            </div>
                        </div>
                        <div class="<?php echo $blue_paper_classes['left_row_text_column']; ?>">
                            <h4 class="mb-2">Years of refining our process.</h4>
                            <p>We know what works in terms of remote collaboration because we’ve experienced, first-hand and painfully, what doesn’t.</p>
                            
                        </div>
                    </div>
                    <hr/>
                    <h3 class="mt-4 pr-2">Think we can help you tackle similar challenges?</h3>
                    <div class="<?php echo $blue_paper_classes['infographic-button-div']; ?>">
                        <a>See our work here</a>
                    </div>
                </article>
            </div>
        </div>



    </div>  
</section>