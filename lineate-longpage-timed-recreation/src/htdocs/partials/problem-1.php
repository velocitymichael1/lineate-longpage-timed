<section class="mt-14">

    <div class="container">

        <div class="row">
                <div class="col-12 d-flex flex-column justify-content-center align-items-center header-background-problem-1  ">
                    <p>Major Problem #1</p>
                    <h2>Overlooking soft Factors.</h2>
                </div>
        </div>
      

        <div class="row d-flex justify-content-center">
            <div class="col-10">
                <article class="<?php echo $white_paper_classes['white_container']; ?>" >
                    <p class="mb-2">Soft skills aren’t just a nice-to-have - they’re integral to the success of any project.</p>
                    <p class="mb-2" >Now, most companies recognize this when it comes to hiring in-house talent.</p>
                    <p class="mb-4">Case in point: <span>92% of hiring professionals reported that soft skills are equally or more important to hire for than hard skills.</span> What’s more, 89% said that when a new hire doesn’t work out, it’s because they lack critical soft skills.</p>
                    <h4 class="mb-6">The thing is: when it comes to outsourcing for a project, soft skills tend to take a firm backseat to technical skills.</h4>
                    <div class="row">
                        <div class="<?php echo $white_paper_classes['text-column']; ?>">
                            <p class="mb-3">And that’s somewhat understandable, right? After all, soft skills are harder to assess than technical skills. Plus, when you’re looking to outsource work to technical specialists, you aren't hiring a peer for life - it’s often short-term. So a lack of soft skills can feel like an acceptable place to lower the bar.</p>
                            <p>BUT that’s a false economy. When you’re trusting an outsider to deliver critical, specialized work (that you don't necessarily have the expertise to manage or measure), that’s when the importance of soft skills rises, not falls.</p>
                        </div>
                        <div class="<?php echo $white_paper_classes['image-column']; ?> ">
                            <img class="white-container-image red-image-border img-fluid" height="344" width="344" src="images/whitepaper-image-1.jpg"/>
                        </div>
                    </div>
                    <p class="mt-6"><strong>If your outsourced team doesn’t have the emotional intelligence, communication skills, good judgement and practical creativity that it takes to deliver an outstanding project while working alongside your in-house team? The options are to push through the dysfunction, scrap the project, or roll the dice on another company.</strong></p>


                </article>
            </div>
        </div>

        <div class="row">
            <div class="col-12">
                <article class="<?php echo $blue_paper_classes['blue_container']; ?>">
                    <h2 class="mb-4">How we cracked it:</h2>

                    <?php //left aligned row?>
                    <div class="<?php echo $blue_paper_classes['left_aligned_row']; ?>">
                        <div class="<?php echo $blue_paper_classes['left_row_image_column']; ?>">
                            <div class="blue-image-holder d-flex justify-content-center align-items-center py-3">
                                <img height="160" width="160" src="images/outline-lineate-icon-hiring.svg" class="img-fluid"/>
                            </div>
                        </div>
                        <div class="<?php echo $blue_paper_classes['left_row_text_column']; ?>">
                            <h4 class="mb-2">We hire with soft skills in mind.</h4>
                            <p>We hire for curiosity. We hire for self-awareness. We hire for a love of collaborative problem-solving.</p>
                        </div>
                    </div>

                    <?php //Right aligned row?>
                    <div class="<?php echo $blue_paper_classes['right_aligned_row']; ?>">
                        <div class="<?php echo $blue_paper_classes['right_row_text_column']; ?>">
                            <h4 class="mb-2">We develop with soft skills in mind.</h4>
                            <p>From Day 1 of onboarding a new hire, we’re developing their soft skill set. We know that soft skills are a crucial ingredient for skilled specialists performing well. You simply can't isolate expertise from empathy in outsourcing - you need the hard stuff (deep domain knowledge) and the soft stuff (being a cultural chameleon).</p>
                        </div>
                        <div class="<?php echo $blue_paper_classes['right_row_image_column']; ?>">
                            <div class="blue-image-holder d-flex justify-content-center align-items-center py-3">
                                <img height="180" width="160" src="images/outline-lineate-icon-dev.svg"/>
                            </div>
                        </div>
                    </div>

                    <?php //Left aligned row?>
                    <div class="<?php echo $blue_paper_classes['left_aligned_row']; ?>">
                        <div class="<?php echo $blue_paper_classes['left_row_image_column']; ?>">
                            <div class="blue-image-holder d-flex justify-content-center align-items-center py-3">
                                <img height="160" width="160" src="images/outline-lineate-icon-team.svg"/>
                            </div>
                        </div>
                        <div class="<?php echo $blue_paper_classes['left_row_text_column']; ?>">
                            <h4 class="mb-2">We know your team are ultimately the ones to impress.</h4>
                            <p>Sometimes, when we’re planning out a project, the client will voice reservations about how their internal team will respond to working with a bunch of strangers.</p>
                            <p>Honestly? Our customers’ in-house teams are often our loudest champions. Our people help them produce excellence in areas where they may not be trained. We take on complex tasks they don’t have time to tackle. It’s not uncommon to see the two teams form a genuine bond.</p>
                        </div>
                    </div>
                    <hr/>
                    <h3 class="mt-4 pr-2">Want to see examples of how we solved this for others?</h3>
                    <div class="<?php echo $blue_paper_classes['infographic-button-div']; ?>">
                        <a>See our work here</a>
                    </div>

                </article>
            </div>
        </div>
    </div>
</section>

 







 



