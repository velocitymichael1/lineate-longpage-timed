<footer class="mt-5 pb-5" style="color: white">
    <div class="container">
        <div class="row d-flex flex-column flex-lg-row align-items-center justify-content-center ">
                <div class="col-6 ">
                    <div class="d-flex justify-content-center justify-content-lg-start align-items-center">
                        <img class="img-fluid" src="images/lineate_reverse_medium 1.png"/>
                    </div>
                </div>
                <div class="col-6">
                    <ul class="links d-flex flex-column flex-lg-row justify-content-center justify-content-lg-between align-items-center mt-2 mr-2 mr-lg-0" >
                        <a class="mb-2 mb-lg-0">Who we are</a>
                        <a class="mb-2 mb-lg-0">What we do</a>
                        <a class="mb-2 mb-lg-0">Our work</a>
                        <a class="mb-2 mb-lg-0">Our community</a>
                        <img src=""/>
                    </ul>
                </div>
        </div>
        <div class="row d-flex justify-content-end align-items-center">
            <div class="col-12 d-flex flex-column justify-content-center justify-content-lg-end align-items-center align-items-lg-end mt-2 mr-lg-3">
                <span>© Copyright 2021 Lineate LLC</span>
                <span>Data Privacy Policy</span>
            </div>
        </div>
    </div>
</footer>
