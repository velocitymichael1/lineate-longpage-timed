<?php
//Imports for class helpers
include 'class-data-helpers/problem-classes.php';

//Imports for lonpage markup
include 'header.php';

include 'partials/hero.php';
include 'partials/text-hero.php';
include 'partials/problem-1.php';
include 'partials/problem-2.php';
include 'partials/problem-3.php';
include 'partials/cta.php';

include 'footer.php';
