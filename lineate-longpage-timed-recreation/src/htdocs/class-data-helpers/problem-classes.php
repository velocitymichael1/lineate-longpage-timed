<?php 
    $white_paper_classes = array(
        "white_container" => "white-container p-2 p-md-6 p-lg-8",
        "image-column" => "col-12 col-lg-6 d-flex justify-content-center align-items-center mt-2 mt-lg-0 pl-lg-4",
        "text-column" => "col-12 col-lg-6 mt-3"
    );

    $blue_paper_classes = array(
        "blue_container" => "blue-container p-2 p-md-4 p-lg-8",
        "left_aligned_row" => "row d-flex flex-row  align-items-center mb-8",
        "right_aligned_row" => "row d-flex flex-row align-items-center justify-content-end mb-8",
        "left_row_text_column" => "col-12 col-lg-6",
        "left_row_image_column" => "col-12 col-lg-3 mb-2 mb-lg-0",
        "right_row_text_column" => "col-12 col-lg-6 order-2 order-lg-1",
        "right_row_image_column" => "col-12 col-lg-3 order-1 order-lg-2 mb-2 mb-lg-0",
        "infographic-button-div" => "infographic-button d-flex justify-content-center align-items-center  mt-2"
    );


?>