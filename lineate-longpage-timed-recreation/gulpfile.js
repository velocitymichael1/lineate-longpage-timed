/**
 * -------------------------------------------------------------------------------------------------
 * Gulp Config
 * -------------------------------------------------------------------------------------------------
 */

const autoprefixer        = require("gulp-autoprefixer"),
    browserSync           = require("browser-sync"),
    changed               = require("gulp-changed"),
    cssnano               = require("cssnano"),
    del                   = require("del"),
    exec                  = require("child_process").exec,
    findup                = require("findup-sync"),
    gulp                  = require("gulp"),
    gulpConfig            = require("./conf/gulp.config.json"),
    gulpSourcemaps        = require("gulp-sourcemaps"),
    gulpTap               = require("gulp-tap"),
    imagemin              = require("gulp-imagemin"),
    path                  = require("path"),
    phpLint               = require("gulp-phplint"),
    postcss               = require("gulp-postcss"),
    postcssFlexbugs       = require("postcss-flexbugs-fixes"),
    purgecss              = require("gulp-purgecss"),
    purgecssWordpress     = require("purgecss-with-wordpress");
    (sass                 = require("gulp-sass")),
    (sizediff             = require("gulp-sizediff")),
    (sassJsonImporter     = require("node-sass-json-importer")),
    (webpackMainConfig    = require("./conf/webpack.config.js")),
    (webpackMerge         = require("webpack-merge")),
    (webpackStream        = require("webpack-stream")),
    (yargs                = require("yargs")),
    (gulpif               = require("gulp-if"));

/*
| Gulp setup
| ==========
*/
const buildDir = gulpConfig.paths.dist,
    config = gulpConfig.config || {},
    output = {},
    project = gulpConfig.project,
    task = yargs.argv._[0];

// Setup path outputs
if (config.platform === "wordpress" || "true" === config.wrapper) {
    if (config.type === "plugin" || "true" === config.wrapper) {
        output.files = buildDir + "wp-content/plugins/" + config.name + "/";
    } else if (config.type === "theme") {
        output.files = buildDir + "wp-content/themes/" + config.name + "/";
    }
} else if (config.platform === "html") {
    output.files = buildDir + "html/";
} else {
    output.files = buildDir + "/";
}

output.build = buildDir + "/";

const verbose = typeof yargs.argv.verbose !== "undefined";

/**
 * -------------------------------------------------------------------------------------------------
 * Gulp Functions
 * -------------------------------------------------------------------------------------------------
 */

function browserServer(done) {
    const browserSyncOptions = {
        logFileChanges: false,
        proxy: config.proxy,
        xip: true,
    };

    if (typeof yargs.argv.nosync === "undefined") {
        browserSync(browserSyncOptions);
    }

    done();
}

/*
| Build JSON
| ==========
| Runs a PHP script to create a JSON file
*/
const buildJson = () => {
    // Execute the PHP script
    return exec("php src/conf/build-json.php", (err, stdout, stderr) => {
        // console.log(stdout)
        // console.log(stderr)
    });
};

/*
| CleanDist
| =========
| Deletes the build output directory
*/
const clean = () => {
    return del([output.files]);
};

/*
| Conf
| ====
| Syncs any required config files
*/
const conf = () => {
    return gulp
        .src(project.conf.src)
        .pipe(changed(output.files + project.conf.dist))
        .pipe(gulp.dest(output.files + project.conf.dist))
        .pipe(browserSync.reload({ stream: true }));
};

const VSBSafelist = [
    // swiper stuff
    "swiper-container-initialized",
    "swiper-container-horizontal",
    "swiper-slide-active",
    "swiper-slide-prev",
    "swiper-slide-next",
    "swiper-container-autoheight",
    // color scheme classes
    /^v-bg(.*)$/,
    /^v-text(.*)$/,
    /^v-heading(.*)$/,
    /^v-overlay-color(.*)$/,
    /^v-btn-set(.*)$/,
    /^v-icon-set(.*)$/,
];

const styleSafelist = VSBSafelist.concat(purgecssWordpress.safelist);

/*
| Styles
| ======
| Compiles all stylesheets from SASS partials
*/
const styles = () => {

    const sassOptions = {
        errLogToConsole: true,
        importer: sassJsonImporter(),
        outputStyle: "expanded",
        includePaths: [
            buildDir + "wp-content/plugins/",
            buildDir + "wp-content/plugins/v-site-base/",
        ],
    };

    const headInLineExclude = function (file) {
        // Return true if match
        return /head-inline/.test(String(file.path));
    };

    return (
        gulp
            .src(project.styles.src)
            .pipe(sass(sassOptions).on("error", sass.logError))
            .pipe(gulpSourcemaps.init())
            .pipe(postcss([postcssFlexbugs, autoprefixer]))
            .pipe(
                gulpif(
                    headInLineExclude,
                    gulpSourcemaps.write(".", { addComment: false }),
                    gulpSourcemaps.write(".")
                )
            )
            .pipe(gulp.dest(output.files + project.styles.dist))
            .pipe(browserSync.reload({ stream: true }))
    );
};

const purgeStyles = () => {
    var sizeDiffFormat = function (data) {
        // stuff available within data object:
        // startSize
        // endSize
        // diff (startSize - endSize)
        // diffPercent (endSize / startSize)
        // compressionRatio (diff / startSize)

        return (
            "- original: " +
            Math.round(data.startSize / 1024) +
            "kb  ---- processed: " +
            Math.round(data.endSize / 1024) +
            "kb ---- savings: " +
            Math.round(data.diff / 1024) +
            "kb (" +
            (100 - Math.round(data.diffPercent * 100)) +
            "%)"
        );
    };

    return gulp
        .src(output.files + project.styles.dist + "*.css")
        .pipe(gulpif(verbose, sizediff.start()))
        .pipe(
            purgecss({
                content: [
                    project.htdocs.src + ".php",
                    project.htdocs.src + ".html",
                    project.htdocs.src + ".json",
                    project.scripts.watch + ".js",
                    project.conf.src,
                    buildDir + "wp-content/plugins/v-site-base/**/*.js",
                    buildDir + "wp-content/plugins/v-site-base/**/*.php",
                    buildDir + "wp-content/plugins/v-site-base/**/*.json",
                    //buildDir + 'wp-content/wphb-cache/cache/**/*.html',
                ],
                safelist: styleSafelist,
                safelistPatterns: purgecssWordpress.safelistPatterns,
                keyframes: true,
            })
        )
        .pipe( postcss([cssnano]) )
        .pipe(
            gulpif(
                verbose,
                sizediff.stop({
                    title: "Purge & nano saving -",
                    showFiles: true,
                    formatFn: sizeDiffFormat,
                })
            )
        )
        .pipe(gulp.dest(output.files + project.styles.dist + "purged/"));
};



const purgeRejected = () => {
    return gulp
        .src(output.files + project.styles.dist + "*.css")
        .pipe(
            purgecss({
                content: [
                    project.htdocs.src + ".php",
                    project.htdocs.src + ".html",
                    project.htdocs.src + ".json",
                    project.scripts.watch + ".js",
                    project.conf.src,
                    buildDir + "wp-content/plugins/v-site-base/**/*.js",
                    buildDir + "wp-content/plugins/v-site-base/**/*.php",
                    buildDir + "wp-content/plugins/v-site-base/**/*.json",
                    //buildDir + 'wp-content/wphb-cache/cache/**/*.html',
                ],
                safelist: styleSafelist,
                safelistPatterns: purgecssWordpress.safelistPatterns,
                keyframes: true,
                rejected: true,
            })
        )
        .pipe(gulp.dest(output.files + project.styles.dist + "rejected/"));
};

/*
| Fonts
| =====
| Syncs all font files
*/
const fonts = () => {
    return gulp
        .src(project.fonts.src)
        .pipe(changed(output.files + project.fonts.dist))
        .pipe(gulp.dest(output.files + project.fonts.dist))
        .pipe(browserSync.reload({ stream: true }));
};

/*
| Htdocs
| ======
| Syncs all HTML, PHP and other template files
*/
const htdocs = () => {
    return gulp
        .src(project.htdocs.src)
        .pipe(changed(output.files + project.htdocs.dist))
        .pipe(
            gulpTap((file) => {
                if (file.path.substr(-3) === "php") {
                    return gulp
                        .src(file.path)
                        .pipe(
                            phpLint("", {
                                skipPassedFiles: true,
                            })
                        )
                        .pipe(
                            phpLint.reporter((file) => {
                                let report = file.phplintReport || {};

                                // Abort the build process if we get a PHP error
                                if (report.error && task !== "watch") {
                                    process.exit(1);
                                }
                            })
                        );
                }
            })
        )
        .pipe(gulp.dest(output.files + project.htdocs.dist))
        .pipe(browserSync.reload({ stream: true }));
};

/*
| Images
| ======
| Compresses all image files and then syncs them
*/
const images = () => {
    return gulp
        .src(project.images.src)
        .pipe(changed(output.files + project.images.dist))
        .pipe(
            gulpTap((file) => {
                if (!file.path.includes("/images/exceptions/")) {
                    return gulp
                        .src(file.path)
                        .pipe(
                            imagemin({
                                optimizationLevel: 3,
                                progressive: true,
                                interlaced: true,
                                svgoPlugins: [
                                    {
                                        removeViewBox: false,
                                        inlineStyles: true,
                                        convertStyleToAttrs: true,
                                    },
                                ],
                            })
                        )
                        .pipe(
                            gulp.dest(
                                output.files + project.images.dist + formatImageDirectory(file)
                            )
                        );
                } else {
                    return gulp
                        .src(file.path)
                        .pipe(
                            gulp.dest(
                                output.files +
                                project.images.dist +
                                formatImageDirectory(file, "/src/assets/images/exceptions")
                            )
                        );
                }
            })
        );
};

formatImageDirectory = (file, path = "/src/assets/images") => {
    const dirnameStrPos = file.dirname.indexOf(path);
    const uniqueDirectory = file.dirname.substr(dirnameStrPos + path.length);

    return uniqueDirectory;
};

/*
| Watcher
| =======
| Watch task for development environment
*/
const watcher = () => {
    gulp.watch(project.blocks.watch, sequenceBlocks);
    gulp.watch(project.conf.watch, sequenceConf);
    gulp.watch(project.fonts.watch, fonts);
    gulp.watch(project.htdocs.watch, htdocs).on("unlink", function (filepath) {
        var watchPathBase = project.htdocs.watch
            .replace("/**/*", "")
            .replace("./", "");
        var destFilePath = filepath.replace(
            watchPathBase,
            output.files.slice(0, -1)
        );
        del.sync(destFilePath);
    });
    gulp.watch(project.images.watch, images);
    gulp.watch(project.scripts.watch, scripts);
    gulp.watch(project.styles.watch, { emit: "all" }, sequenceStyles);
};

/*
| Scripts
| =======
| Handles all JS script compilation
*/
const scripts = () => {
    return gulp.src(project.scripts.src).pipe(
        gulpTap((file) => {
            const webpackOptions = {
                entry: { [file.basename]: path.resolve(file.path) },
                mode: task === "watch" ? "development" : "production",
                output: {
                    filename: "[name]",
                },
            };

            return webpackStream(webpackMerge(webpackMainConfig, webpackOptions))
                .pipe(gulp.dest(output.files + project.scripts.dist))
                .pipe(browserSync.reload({ stream: true }));
        })
    );
};

/**
 * -------------------------------------------------------------------------------------------------
 * Gulp Sequences
 * -------------------------------------------------------------------------------------------------
 */

const sequenceStyles = gulp.series(styles, purgeStyles, purgeRejected, htdocs);

// Sequence to execute when block files are changed
const sequenceBlocks = gulp.series(conf, htdocs, scripts);

// Sequence to run when building the project (i.e. making it production-ready)
let sequenceBuild = {};

if (config.type === "grader") {
    sequenceBuild = gulp.series(
        clean,
        conf,
        fonts,
        buildJson,
        htdocs,
        images,
        sequenceStyles,
        scripts
    );
} else {
    sequenceBuild = gulp.series(
        clean,
        conf,
        fonts,
        htdocs,
        images,
        sequenceStyles,
        scripts
    );
}

// Sequence to execute when conf files are changed
const sequenceConf = gulp.series(conf, htdocs, sequenceStyles);

// Sequence to follow watch is active
const sequenceWatch = gulp.parallel(watcher, browserServer);

/**
 * -------------------------------------------------------------------------------------------------
 * Gulp Tasks
 * -------------------------------------------------------------------------------------------------
 */

// Expose the following tasks to the command line
exports.blocks = sequenceBlocks;
exports.build = sequenceBuild;
exports.conf = sequenceConf;
exports.fonts = fonts;
exports.htdocs = htdocs;
exports.scripts = scripts;
exports.images = images;
exports.styles = sequenceStyles;
exports.watch = sequenceWatch;
exports.purgeRejected = purgeRejected;
exports.purgeStyles = purgeStyles;
exports.buildJson = buildJson;